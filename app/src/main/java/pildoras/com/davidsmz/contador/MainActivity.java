package pildoras.com.davidsmz.contador;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public int contador = 0;
    TextView vistaContador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        vistaContador = findViewById(R.id.textViewContador);
        contador = 0;
    }

    public void incrementar(View vista) {
        contador++;
        vistaContador.setText("" + contador);
    }

    public void resetear(View vista) {
        EditText numeroReseteo = findViewById(R.id.numberReset);

        try {
            contador = Integer.parseInt(numeroReseteo.getText().toString());
        }catch (Exception e) {
            contador = 0;
        }
        numeroReseteo.setText("");
        vistaContador.setText("" + contador);

        InputMethodManager miTeclado = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        miTeclado.hideSoftInputFromWindow(numeroReseteo.getWindowToken(), 0);
    }

    public void disminuir(View vista) {
        contador--;
        if (contador < 0) {
            CheckBox permitirNegativo = findViewById(R.id.countNegative);
            if (!permitirNegativo.isChecked()) {
                contador = 0;
            }
        }

        vistaContador.setText("" + contador);
    }



//    public void mostrarContador() {
//        TextView vistaContador = findViewById(R.id.textViewContador);
//        vistaContador.setText("" + contador);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
